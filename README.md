<h1 align="center">Welcome to FPGA Kmer Sketch </h1>
<p>
  <a href="#" target="_blank">
    <img alt="License: GNU GPLv3.0" src="https://img.shields.io/badge/License-GNU GPLv3.0-yellow.svg" />
  </a>
</p>

Software and FPGA-based hardware acceleration platforms for mining discriminative k-mers in large DNA sequences. K-mer mining based on using estimated frequency counts obtained thorugh Sketch-based streaming counting algorithm. Implementations as described on <b><i>Mining Discriminative K-mers in DNA Sequences Using Sketches and Hardware Acceleration</i></b>, published and available on <a href="https://ieeeaccess.ieee.org/"><i>IEEE Access</i></a> journal.

## Publication URL
<b>"Mining Discriminative K-Mers in DNA Sequences Using Sketches and Hardware Acceleration"</b> in IEEE Access, vol. 8, pp. 114715-114732

<a href="https://ieeexplore.ieee.org/document/9122013">doi: 10.1109/ACCESS.2020.3003918</a>


## Authors
**Antonio Saavedra, Hans Lehnert, Cecilia Hernández, Gonzalo Carvajal and Miguel Figueroa**

