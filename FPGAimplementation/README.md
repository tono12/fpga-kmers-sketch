<h1 align="center">FPGA-Accelerator Implementation </h1>
<p>
  <a href="#" target="_blank">
    <img alt="License: GPLv3.0" src="https://img.shields.io/badge/License-GPLv3.0-yellow.svg" />
  </a>
</p>

This example implements a 4x32k sketch with the thresholds set to test with AR-1000 database. The example is to be integrated as an RTL Kernel into Xilinx SDAccel (SDx) environment, and has been tested on a Xilinx KCU1500 Acceleration Development Kit.

## File List

The following SystemVerilog files are included:
- 19 Sketches SV files (kXXsketch.sv)
- 19 Kmer caches SV files (kXXkmercache.sv)
- 19 H3Hash modules for sketch inputs SV files (kXXh3sketch.sv)
- 19 H3Hash modules for cache inputs SV files (kXXh3cache.sv)
- Minimum computing module SV file (minimum.sv)
- Module to read and separate inputs to different sketches SV file (inputdriver.sv)
- Module to write results back SV file (backwriter.sv)
- AXI interface to read from SDx RTL Kernel Mem Interface (modified SDx auto generated file)
- AXI interface to write to SDx RTL Kernel Mem Interface (modified SDx auto generated file)
- Top module that integrates the sketches with the Memory Interfaces (topRTL.sv)

Also included:
- Example SDx C code template (mainsdx.c)

## Usage

Fore integrating into SDx as an RTL Kernel.

The top module should be integrated where appropriated as indicated by SDx RTL Kernel integration wizard. Additionally, auto-generated AXI interfaces should be replaced by the provided, or adjusted to desired communication signals widths and depths.

For the Kernel to synthesize in Vivado, Memory Block IP instances should be created and integrated into the project. kXXsketch.sv and kXXkmercache.sv expect Memory Block IP with the following names and configurations:

- <b>memrowAAWBB</b>: where AA is the numbers of bits per counter and BB is the address width of each sketch row. Expected Memory Block IP configuration includes: One write-only port A, with Write enable signal; One always enable read-only port B; and no Primitive Output Register, to adjust to the expected latency.

- <b>cachekCC</b>: where CC is the kmer length k. It should be able to store 256 sets of (k*2+AA) bits, where AA is the number of bits per sketch counter. The same Memory Block IP configurations as for memrowAAWBB memorys are expected.

For example, in the included sources, AA is set to 20, BB is 16, and k goes from 10 to 28. With this values, the sketch for k=10 would expect BlockRAMs memories named <i>memrow20W16</i> with the described configuration. That same sketch would also require a cache set named <i>cachek10</i>.

Additionally, a BlockRAM input buffer should also be created to store incoming communication for the inputdriver.sv file.