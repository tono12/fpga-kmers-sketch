`timescale 1ps / 1ps

module backwriter #(
  parameter integer C_AXIS_TDATA_WIDTH = 512 // Data width of both input and output data
)
(
  input wire                           aclk,
  input wire                           aresetn,

  input wire                           write_start,
  input logic [511:0]                  datain,
  output logic [5:0]                   cachequery,
  output logic                         changecache,

  output reg                           m_axis_tvalid,
  input wire                           m_axis_tready,
  output wire [C_AXIS_TDATA_WIDTH-1:0] m_axis_tdata
);

/////////////////////////////////////////////////////////////////////////////
// Variables
/////////////////////////////////////////////////////////////////////////////
   
logic [C_AXIS_TDATA_WIDTH-1:0] data_out;
logic                          areset = 1'b0;

logic [1:0]                    fsm_state;
logic                          late_start;
logic [2:0]                    cachecnt;

/////////////////////////////////////////////////////////////////////////////
// Adder Logic
/////////////////////////////////////////////////////////////////////////////

always @(posedge aclk) begin
  areset <= ~aresetn;
end

always @(posedge aclk) begin
   if (areset) begin
      m_axis_tvalid <= 1'b0;
      cachequery <= 6'd0;
      late_start <= 1'b0;
   end
end 

always_ff @(posedge aclk) begin
   unique case (fsm_state)
      2'b00: begin
         if (write_start || late_start) begin
            fsm_state <= 2'b01;
            cachequery <= 6'd1;
            m_axis_tvalid <= 1'b1;
            late_start <= 1'b0;
         end
      end
      2'b01: begin
         data_out <= datain;
         cachequery <= cachequery + 1'b1;
         m_axis_tvalid <= 1'b1;
         if (cachequery == 6'd63) begin
            changecache <= 1'b1;
            fsm_state <= 2'b10;
         end
      end // case: 2'b01
      2'b10: begin
         data_out <= datain;
         m_axis_tvalid <= 1'b0;
         fsm_state <= 2'b00;
         changecache <= 1'b0;
         cachecnt <= cachecnt + 1'b1;
         if (cachecnt == 3'd5) begin
            late_start <= 1'b0;
         end
         else begin
            late_start <= 1'b1;
         end
      end // case: 2'b10
   endcase // unique case (fsm_state)
end // always @ (posedge aclk)

assign m_axis_tdata = data_out;

endmodule
