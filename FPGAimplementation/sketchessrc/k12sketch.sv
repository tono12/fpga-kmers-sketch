module sketch12(
       input  logic        clk,
       input  logic [23:0] datain,
       input  logic        dataval,
       input  logic        resetn,
       output logic        validHH,
       output logic [43:0] dataout
    );


   logic [17:0] outa[0:3];
   logic        avalid[0:3];

   logic [17:0] rrow_addr[0:3];
   logic [17:0] wrow_addr[0:3];
   logic [19:0]  w_data[0:3];
   logic [19:0]  r_data[0:3];
   logic        rowwea[0:3];

   logic [17:0] save_addr[0:3];
   logic [17:0] save_addr2[0:3];
   logic        save_val[0:3];
   logic        save_val2[0:3];
   logic        save_val3[0:3];
   logic [19:0]  save_data[0:3];
   logic [19:0]  save_data2[0:3];

   logic [19:0]  est;
   logic        hhalert;

   logic [23:0] kmerSR0, kmerSR1, kmerSR2, kmerSR3, kmerSR4, kmerSR5, kmerSR6, kmerSR7;
   logic        valSR0, valSR1, valSR2, valSR3, valSR4, valSR5, valSR6, valSR7;

   parameter [431:0] h3sa0 = 432'b101110010000010000011111011101011011011111101111101111111010100111011001011111111111001011011101111110110100011001000000100101101111000110001010111111100100100010001101100010010010001110010010011100110001101010000000001010110001111101010101110110011100110110101110101100000101110001110001010001100000101101000010110100111101011000011110101010100001110111111000001011001001000100001100001101110011111011001110010111111010110110001010;
   parameter [431:0] h3sa1 = 432'b100100001000000100001011110111011000010000001010111110101101110001001110001000011100011101101000111100110001000100111101000111001010101111110000010001100100110100101110111000010100001001100110001100011101100010001010000100101111100110101000110111101010011100011101010100100101111101101101101101110000100100101001110101000100001000011101100100001110100101110101101100100011001100110111100111111001010100011100100100010100000110110101;
   parameter [431:0] h3sa2 = 432'b100000101000010000110001110101000101010011000010101001110111001100100110001000010101100010001100110001010111011100011111100100001010100000111011100101100001100011010110100111011001101011001000010111011001011101100111001101111101000000000110100000110010000111010100011001010110101111100101010101110110101010101001110100110101100101110011111100011111000000100001111101010011000100001110001100111101001010100111101110010001100110010101;
   parameter [431:0] h3sa3 = 432'b010000111110101100011100110110011011101001110101011110011011111010001001110100001100111000010110010000100100010101101000001000010001010010001001111000010111111000000111110010011101111001011000111101100100110001111001110101000110010100110111111000011011101111100111110001111110101011000010101110111010110000010111111111101110100111100001110101100010000011111110111100111010010110010000010111101011100011011100001011111111111111100011;

   h3hash12 #(.sds(h3sa0)) hashaddr0 (.clk(clk), .resetn, .kin(datain), .inval(dataval), .outval(avalid[0]), .respos(outa[0]));
   h3hash12 #(.sds(h3sa1)) hashaddr1 (.clk(clk), .resetn, .kin(datain), .inval(dataval), .outval(avalid[1]), .respos(outa[1]));
   h3hash12 #(.sds(h3sa2)) hashaddr2 (.clk(clk), .resetn, .kin(datain), .inval(dataval), .outval(avalid[2]), .respos(outa[2]));
   h3hash12 #(.sds(h3sa3)) hashaddr3 (.clk(clk), .resetn, .kin(datain), .inval(dataval), .outval(avalid[3]), .respos(outa[3]));

   memrow20W18 sketchrow0 (.clka(clk), .wea(rowwea[0]), .addra(wrow_addr[0]), .dina(w_data[0]), .clkb(clk), .addrb(rrow_addr[0]), .doutb(r_data[0]));
   memrow20W18 sketchrow1 (.clka(clk), .wea(rowwea[1]), .addra(wrow_addr[1]), .dina(w_data[1]), .clkb(clk), .addrb(rrow_addr[1]), .doutb(r_data[1]));
   memrow20W18 sketchrow2 (.clka(clk), .wea(rowwea[2]), .addra(wrow_addr[2]), .dina(w_data[2]), .clkb(clk), .addrb(rrow_addr[2]), .doutb(r_data[2]));
   memrow20W18 sketchrow3 (.clka(clk), .wea(rowwea[3]), .addra(wrow_addr[3]), .dina(w_data[3]), .clkb(clk), .addrb(rrow_addr[3]), .doutb(r_data[3]));

   minimum estim (.clk, .in0(r_data[0]), .in1(r_data[1]), .in2(r_data[2]), .in3(r_data[3]), .out(est));

   assign hhalert = (est >= 45000)? 1:0;

   genvar  i;

   generate
      for (i = 0; i<4; i++) begin
         assign rrow_addr[i] = outa[i];
         assign rowwea[i] = ((save_data2[i] == est) && save_val3[i]);
         always_ff @(posedge clk) begin
            save_addr[i] <= outa[i];
            save_val[i]  <= avalid[i];

            save_data[i]  <= r_data[i];
            save_addr2[i] <= save_addr[i];
            save_val2[i]  <= save_val[i];

            w_data[i] <= save_data[i]+1;
            wrow_addr[i] <= save_addr2[i];
            save_val3[i] <= save_val2[i];
            save_data2[i] <= save_data[i];
         end // always_ff @ (posedge clk)
      end // for (i = 0; i<4; i++)
   endgenerate

   always_ff @(posedge clk) begin
      kmerSR0 <= datain;
      kmerSR1 <= kmerSR0;
      kmerSR2 <= kmerSR1;
      kmerSR3 <= kmerSR2;
      kmerSR4 <= kmerSR3;
      kmerSR5 <= kmerSR4;
      kmerSR6 <= kmerSR5;
      kmerSR7 <= kmerSR6;

      valSR0 <= dataval;
      valSR1 <= valSR0;
      valSR2 <= valSR1;
      valSR3 <= valSR2;
      valSR4 <= valSR3;
      valSR5 <= valSR4;
      valSR6 <= valSR5;
      valSR7 <= valSR6;

      validHH <= (hhalert && valSR7)? 1'b1 : 1'b0;
      dataout <= {est, kmerSR7};
   end // always_ff @ (posedge clk)
endmodule // sketch
