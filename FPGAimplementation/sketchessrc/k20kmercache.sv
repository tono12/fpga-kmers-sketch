module cachecontroller20(
       input  logic         clk,
       input  logic         datavalid,
       input  logic         state,
       input  logic [7:0]   queryin,
       input  logic [7:0]   resetn,
       input  logic [59:0]  datain,
       output logic [255:0] outres
    );


   logic [7:0]  setaddr, readaddr, addrb;
   logic        inval;
   logic        outval;
   logic [3:0]  wen, wea;
   logic [7:0]  w_addr;
   logic [59:0] w_data;
   logic [59:0] r_data[0:3];
   logic [3:0]  hit;
   logic [1:0]  ccounter[0:255];
   logic [59:0] cacheSR0, cacheSR1, cacheSR2, cacheSR3, cacheSR4, cacheSR5, cacheSR6;
   logic [7:0]  addressSR0;
   logic        validSR0;

   parameter [319:0] h3scache = 320'b11110101000000010000000100010011010010110010000100000111000100010000010100000001111010010000000010111111010111111111111100000011001111110000101100010101001111010100011100000011001110011110010100100001001101111000111111111011011011110000110100111101011000010010100101100101100110110001000100000101010010011010000100110101;

   h3cache20 #(.sds(h3scache)) h3cache (.clk(clk), .kin(datain[39:0]), .resetn, .inval(datavalid), .outval(outval), .respos(setaddr));

   cachek20 cacheset0 (.clka(clk), .wea(wea[0]), .addra(w_addr), .dina(w_data), .clkb(clk), .addrb, .doutb(r_data[0]));
   cachek20 cacheset1 (.clka(clk), .wea(wea[1]), .addra(w_addr), .dina(w_data), .clkb(clk), .addrb, .doutb(r_data[1]));
   cachek20 cacheset2 (.clka(clk), .wea(wea[2]), .addra(w_addr), .dina(w_data), .clkb(clk), .addrb, .doutb(r_data[2]));
   cachek20 cacheset3 (.clka(clk), .wea(wea[3]), .addra(w_addr), .dina(w_data), .clkb(clk), .addrb, .doutb(r_data[3]));

   always_ff @(posedge clk) begin
      cacheSR0 <= datain;
      cacheSR1 <= cacheSR0;
      cacheSR2 <= cacheSR1;
      cacheSR3 <= cacheSR2;
      cacheSR4 <= cacheSR3;
      cacheSR5 <= cacheSR4;
      cacheSR6 <= cacheSR5;

      addressSR0 <= setaddr;
      validSR0 <= outval;

      if (validSR0) begin
         if (hit == 4'b0000) begin
            unique casez (ccounter[addressSR0]) 
               2'b00 : wen <= 4'b1000;
               2'b01 : wen <= 4'b0100;
               2'b10 : wen <= 4'b0010;
               2'b11 : wen <= 4'b0001;
            endcase
            ccounter[addressSR0] <= ccounter[addressSR0] + 1;
         end // if (hit == 3'b000)
         else if (hit == 4'b1111)
            wen <= 4'b0000;
         else
            wen <= hit;
         w_data <= cacheSR6;
         w_addr <= addressSR0;
      end // if (validSR0)
      else begin
         wen <= 4'b0000;
      end // else: !if(validSR0)
   end // always_ff @ (posedge clk)

   assign addrb = (state)? readaddr:setaddr;
   assign wea = (state)? 4'b0000:wen;

   always_comb begin
      readaddr =  queryin;
      outres = {4'd0, r_data[0], 4'd0, r_data[1], 4'd0, r_data[2], 4'd0, r_data[3]};
   end
   genvar i, k;
   generate
      for (i = 0; i<4; i++) begin
         assign hit[i] = (r_data[i][39:0] == cacheSR6[39:0]);
      end // for (i = 0; i<2; i++)
   endgenerate

   generate
      for (k = 0; k<256; k++) begin
         initial ccounter[k] = 0;
      end // for (k++)
   endgenerate
endmodule // sketch
