module h3cache17 #(parameter sds=0)(
      input  logic          clk,
      input  logic [33:0]   kin,
      input  logic          inval,
      input  logic          resetn,
      output logic          outval,
      output logic [7:0]   respos
);

   genvar i,j,k,l,m;

   logic [7:0] firstxor[0:16];
   logic [7:0] seconxor[0:8];
   logic [7:0] thirdxor[0:4];
   logic [7:0] fourtxor[0:2];
   logic [7:0] fifthxor[0:1];
   logic [7:0] seeds[0:33];
   logic        lastrst;
   logic        valSR0, valSR1, valSR2, valSR3, valSR4;

   generate
      for (i=0; i<17; i++) begin
        always_ff @(posedge clk) begin
           unique case (kin[33-i*2:32-i*2])
             2'b00: firstxor[i] <= 0;
             2'b01: firstxor[i] <= seeds[i*2+1];
             2'b10: firstxor[i] <= seeds[i*2];
             2'b11: firstxor[i] <= seeds[i*2]^seeds[i*2+1];
           endcase // unique case (key[i+1:i])
        end
      end
      for (j=0; j<8; j++) begin
         always_ff @(posedge clk) begin
            seconxor[j] <= firstxor[j*2]^firstxor[j*2+1];
         end
      end
      for (k=0; k<4; k++) begin
         always_ff @(posedge clk) begin
            thirdxor[k] <= seconxor[k*2]^seconxor[k*2+1];
         end
      end
      for (l=0; l<2; l++) begin
         always_ff @(posedge clk) begin
            fourtxor[l] <= thirdxor[l*2]^thirdxor[l*2+1];
         end
      end
      for (m=0; m<1; m++) begin
         always_ff @(posedge clk) begin
            fifthxor[m] <= fourtxor[m*2]^fourtxor[m*2+1];
         end
      end
   endgenerate

   always_ff @(posedge clk) begin
      seconxor[8] <= firstxor[16];
      thirdxor[4] <= seconxor[8];
      fourtxor[2] <= thirdxor[4];
      fifthxor[1] <= fourtxor[2];
      respos <= fifthxor[0]^fifthxor[1];
      valSR0 <= inval;
      valSR1 <= valSR0;
      valSR2 <= valSR1;
      valSR3 <= valSR2;
      valSR4 <= valSR3;
      outval <= valSR4;
   end // always_ff

   always_ff @(posedge clk) begin
      if(!resetn && lastrst) begin
         seeds[0] <= sds[7:0];
         seeds[1] <= sds[15:8];
         seeds[2] <= sds[23:16];
         seeds[3] <= sds[31:24];
         seeds[4] <= sds[39:32];
         seeds[5] <= sds[47:40];
         seeds[6] <= sds[55:48];
         seeds[7] <= sds[63:56];
         seeds[8] <= sds[71:64];
         seeds[9] <= sds[79:72];
         seeds[10] <= sds[87:80];
         seeds[11] <= sds[95:88];
         seeds[12] <= sds[103:96];
         seeds[13] <= sds[111:104];
         seeds[14] <= sds[119:112];
         seeds[15] <= sds[127:120];
         seeds[16] <= sds[135:128];
         seeds[17] <= sds[143:136];
         seeds[18] <= sds[151:144];
         seeds[19] <= sds[159:152];
         seeds[20] <= sds[167:160];
         seeds[21] <= sds[175:168];
         seeds[22] <= sds[183:176];
         seeds[23] <= sds[191:184];
         seeds[24] <= sds[199:192];
         seeds[25] <= sds[207:200];
         seeds[26] <= sds[215:208];
         seeds[27] <= sds[223:216];
         seeds[28] <= sds[231:224];
         seeds[29] <= sds[239:232];
         seeds[30] <= sds[247:240];
         seeds[31] <= sds[255:248];
         seeds[32] <= sds[263:256];
         seeds[33] <= sds[271:264];
      end // if(!resetn && lastrst)
      lastrst <= resetn;
   end // always_ff
endmodule
