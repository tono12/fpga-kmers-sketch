module cachecontroller17(
       input  logic         clk,
       input  logic         datavalid,
       input  logic         state,
       input  logic [7:0]   queryin,
       input  logic [7:0]   resetn,
       input  logic [53:0]  datain,
       output logic [255:0] outres
    );


   logic [7:0]  setaddr, readaddr, addrb;
   logic        inval;
   logic        outval;
   logic [3:0]  wen, wea;
   logic [7:0]  w_addr;
   logic [53:0] w_data;
   logic [53:0] r_data[0:3];
   logic [3:0]  hit;
   logic [1:0]  ccounter[0:255];
   logic [53:0] cacheSR0, cacheSR1, cacheSR2, cacheSR3, cacheSR4, cacheSR5, cacheSR6;
   logic [7:0]  addressSR0;
   logic        validSR0;

   parameter [271:0] h3scache = 272'b01011001101110110001010100101011000100010000110111110111111110111010100100101001010010011100010111111101001110011100110100000001001010011011011101001101000111010001111111011011001101010110110100000101000101110010001111000111000010111001011100111101101001010000001100010101;

   h3cache17 #(.sds(h3scache)) h3cache (.clk(clk), .kin(datain[33:0]), .resetn, .inval(datavalid), .outval(outval), .respos(setaddr));

   cachek17 cacheset0 (.clka(clk), .wea(wea[0]), .addra(w_addr), .dina(w_data), .clkb(clk), .addrb, .doutb(r_data[0]));
   cachek17 cacheset1 (.clka(clk), .wea(wea[1]), .addra(w_addr), .dina(w_data), .clkb(clk), .addrb, .doutb(r_data[1]));
   cachek17 cacheset2 (.clka(clk), .wea(wea[2]), .addra(w_addr), .dina(w_data), .clkb(clk), .addrb, .doutb(r_data[2]));
   cachek17 cacheset3 (.clka(clk), .wea(wea[3]), .addra(w_addr), .dina(w_data), .clkb(clk), .addrb, .doutb(r_data[3]));

   always_ff @(posedge clk) begin
      cacheSR0 <= datain;
      cacheSR1 <= cacheSR0;
      cacheSR2 <= cacheSR1;
      cacheSR3 <= cacheSR2;
      cacheSR4 <= cacheSR3;
      cacheSR5 <= cacheSR4;
      cacheSR6 <= cacheSR5;

      addressSR0 <= setaddr;
      validSR0 <= outval;

      if (validSR0) begin
         if (hit == 4'b0000) begin
            unique casez (ccounter[addressSR0]) 
               2'b00 : wen <= 4'b1000;
               2'b01 : wen <= 4'b0100;
               2'b10 : wen <= 4'b0010;
               2'b11 : wen <= 4'b0001;
            endcase
            ccounter[addressSR0] <= ccounter[addressSR0] + 1;
         end // if (hit == 3'b000)
         else if (hit == 4'b1111)
            wen <= 4'b0000;
         else
            wen <= hit;
         w_data <= cacheSR6;
         w_addr <= addressSR0;
      end // if (validSR0)
      else begin
         wen <= 4'b0000;
      end // else: !if(validSR0)
   end // always_ff @ (posedge clk)

   assign addrb = (state)? readaddr:setaddr;
   assign wea = (state)? 4'b0000:wen;

   always_comb begin
      readaddr =  queryin;
      outres = {10'd0, r_data[0], 10'd0, r_data[1], 10'd0, r_data[2], 10'd0, r_data[3]};
   end
   genvar i, k;
   generate
      for (i = 0; i<4; i++) begin
         assign hit[i] = (r_data[i][33:0] == cacheSR6[33:0]);
      end // for (i = 0; i<2; i++)
   endgenerate

   generate
      for (k = 0; k<256; k++) begin
         initial ccounter[k] = 0;
      end // for (k++)
   endgenerate
endmodule // sketch
