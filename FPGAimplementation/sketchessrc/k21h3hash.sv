module h3hash21 #(parameter sds=0)(
      input  logic          clk,
      input  logic [41:0]   kin,
      input  logic          inval,
      input  logic          resetn,
      output logic          outval,
      output logic [17:0]   respos
);

   genvar i,j,k,l,m;

   logic [17:0] firstxor[0:20];
   logic [17:0] seconxor[0:10];
   logic [17:0] thirdxor[0:5];
   logic [17:0] fourtxor[0:2];
   logic [17:0] fifthxor[0:1];
   logic [17:0] seeds[0:41];
   logic        lastrst;
   logic        valSR0, valSR1, valSR2, valSR3, valSR4;

   generate
      for (i=0; i<21; i++) begin
        always_ff @(posedge clk) begin
           unique case (kin[41-i*2:40-i*2])
             2'b00: firstxor[i] <= 0;
             2'b01: firstxor[i] <= seeds[i*2+1];
             2'b10: firstxor[i] <= seeds[i*2];
             2'b11: firstxor[i] <= seeds[i*2]^seeds[i*2+1];
           endcase // unique case (key[i+1:i])
        end
      end
      for (j=0; j<10; j++) begin
         always_ff @(posedge clk) begin
            seconxor[j] <= firstxor[j*2]^firstxor[j*2+1];
         end
      end
      for (k=0; k<5; k++) begin
         always_ff @(posedge clk) begin
            thirdxor[k] <= seconxor[k*2]^seconxor[k*2+1];
         end
      end
      for (l=0; l<3; l++) begin
         always_ff @(posedge clk) begin
            fourtxor[l] <= thirdxor[l*2]^thirdxor[l*2+1];
         end
      end
      for (m=0; m<1; m++) begin
         always_ff @(posedge clk) begin
            fifthxor[m] <= fourtxor[m*2]^fourtxor[m*2+1];
         end
      end
   endgenerate

   always_ff @(posedge clk) begin
      seconxor[10] <= firstxor[20];
      thirdxor[5] <= seconxor[10];
      fifthxor[1] <= fourtxor[2];
      respos <= fifthxor[0]^fifthxor[1];
      valSR0 <= inval;
      valSR1 <= valSR0;
      valSR2 <= valSR1;
      valSR3 <= valSR2;
      valSR4 <= valSR3;
      outval <= valSR4;
   end // always_ff

   always_ff @(posedge clk) begin
      if(!resetn && lastrst) begin
         seeds[0] <= sds[17:0];
         seeds[1] <= sds[35:18];
         seeds[2] <= sds[53:36];
         seeds[3] <= sds[71:54];
         seeds[4] <= sds[89:72];
         seeds[5] <= sds[107:90];
         seeds[6] <= sds[125:108];
         seeds[7] <= sds[143:126];
         seeds[8] <= sds[161:144];
         seeds[9] <= sds[179:162];
         seeds[10] <= sds[197:180];
         seeds[11] <= sds[215:198];
         seeds[12] <= sds[233:216];
         seeds[13] <= sds[251:234];
         seeds[14] <= sds[269:252];
         seeds[15] <= sds[287:270];
         seeds[16] <= sds[305:288];
         seeds[17] <= sds[323:306];
         seeds[18] <= sds[341:324];
         seeds[19] <= sds[359:342];
         seeds[20] <= sds[377:360];
         seeds[21] <= sds[395:378];
         seeds[22] <= sds[413:396];
         seeds[23] <= sds[431:414];
         seeds[24] <= sds[449:432];
         seeds[25] <= sds[467:450];
         seeds[26] <= sds[485:468];
         seeds[27] <= sds[503:486];
         seeds[28] <= sds[521:504];
         seeds[29] <= sds[539:522];
         seeds[30] <= sds[557:540];
         seeds[31] <= sds[575:558];
         seeds[32] <= sds[593:576];
         seeds[33] <= sds[611:594];
         seeds[34] <= sds[629:612];
         seeds[35] <= sds[647:630];
         seeds[36] <= sds[665:648];
         seeds[37] <= sds[683:666];
         seeds[38] <= sds[701:684];
         seeds[39] <= sds[719:702];
         seeds[40] <= sds[737:720];
         seeds[41] <= sds[755:738];
      end // if(!resetn && lastrst)
      lastrst <= resetn;
   end // always_ff
endmodule
