module h3hash13 #(parameter sds=0)(
      input  logic          clk,
      input  logic [25:0]   kin,
      input  logic          inval,
      input  logic          resetn,
      output logic          outval,
      output logic [17:0]   respos
);

   genvar i,j,k,l,m;

   logic [17:0] firstxor[0:12];
   logic [17:0] seconxor[0:6];
   logic [17:0] thirdxor[0:3];
   logic [17:0] fourtxor[0:1];
   logic [17:0] seeds[0:25];
   logic        lastrst;
   logic        valSR0, valSR1, valSR2, valSR3;

   generate
      for (i=0; i<13; i++) begin
        always_ff @(posedge clk) begin
           unique case (kin[25-i*2:24-i*2])
             2'b00: firstxor[i] <= 0;
             2'b01: firstxor[i] <= seeds[i*2+1];
             2'b10: firstxor[i] <= seeds[i*2];
             2'b11: firstxor[i] <= seeds[i*2]^seeds[i*2+1];
           endcase // unique case (key[i+1:i])
        end
      end
      for (j=0; j<6; j++) begin
         always_ff @(posedge clk) begin
            seconxor[j] <= firstxor[j*2]^firstxor[j*2+1];
         end
      end
      for (k=0; k<3; k++) begin
         always_ff @(posedge clk) begin
            thirdxor[k] <= seconxor[k*2]^seconxor[k*2+1];
         end
      end
      for (l=0; l<2; l++) begin
         always_ff @(posedge clk) begin
            fourtxor[l] <= thirdxor[l*2]^thirdxor[l*2+1];
         end
      end
   endgenerate

   always_ff @(posedge clk) begin
      seconxor[6] <= firstxor[12];
      thirdxor[3] <= seconxor[6];
      respos <= fourtxor[0]^fourtxor[1];
      valSR0 <= inval;
      valSR1 <= valSR0;
      valSR2 <= valSR1;
      valSR3 <= valSR2;
      outval <= valSR3;
   end // always_ff

   always_ff @(posedge clk) begin
      if(!resetn && lastrst) begin
         seeds[0] <= sds[17:0];
         seeds[1] <= sds[35:18];
         seeds[2] <= sds[53:36];
         seeds[3] <= sds[71:54];
         seeds[4] <= sds[89:72];
         seeds[5] <= sds[107:90];
         seeds[6] <= sds[125:108];
         seeds[7] <= sds[143:126];
         seeds[8] <= sds[161:144];
         seeds[9] <= sds[179:162];
         seeds[10] <= sds[197:180];
         seeds[11] <= sds[215:198];
         seeds[12] <= sds[233:216];
         seeds[13] <= sds[251:234];
         seeds[14] <= sds[269:252];
         seeds[15] <= sds[287:270];
         seeds[16] <= sds[305:288];
         seeds[17] <= sds[323:306];
         seeds[18] <= sds[341:324];
         seeds[19] <= sds[359:342];
         seeds[20] <= sds[377:360];
         seeds[21] <= sds[395:378];
         seeds[22] <= sds[413:396];
         seeds[23] <= sds[431:414];
         seeds[24] <= sds[449:432];
         seeds[25] <= sds[467:450];
      end // if(!resetn && lastrst)
      lastrst <= resetn;
   end // always_ff
endmodule
