module sketch15(
       input  logic        clk,
       input  logic [29:0] datain,
       input  logic        dataval,
       input  logic        resetn,
       output logic        validHH,
       output logic [49:0] dataout
    );


   logic [17:0] outa[0:3];
   logic        avalid[0:3];

   logic [17:0] rrow_addr[0:3];
   logic [17:0] wrow_addr[0:3];
   logic [19:0]  w_data[0:3];
   logic [19:0]  r_data[0:3];
   logic        rowwea[0:3];

   logic [17:0] save_addr[0:3];
   logic [17:0] save_addr2[0:3];
   logic        save_val[0:3];
   logic        save_val2[0:3];
   logic        save_val3[0:3];
   logic [19:0]  save_data[0:3];
   logic [19:0]  save_data2[0:3];

   logic [19:0]  est;
   logic        hhalert;

   logic [29:0] kmerSR0, kmerSR1, kmerSR2, kmerSR3, kmerSR4, kmerSR5, kmerSR6, kmerSR7;
   logic        valSR0, valSR1, valSR2, valSR3, valSR4, valSR5, valSR6, valSR7;

   parameter [539:0] h3sa0 = 540'b011001101111100000100011010101111100010011001010100000011011000111100001000000001001111000101010101100111010101110101111000101001011000100001001010101000110110011101000111000010000110101010101110100100110011011101001010001011101000110011100000111001000110000001100111001000001101001101001001101101110110110010011101101011001011100010011010100001000010001101001010100011010011010010010110010001001111111010010000110100100111010001111110001100100101011001011010001101011111000010110101111100100111111010111001001100100001010110001001111000000;
   parameter [539:0] h3sa1 = 540'b111000100010100010111111100110001101110111110000100111101010110101001100111101111101111101011101111101100011100111101001110010000101101001000011001101000000011010000010101001101111101110010110101010110110011100011110000001100110110010100001000010111010101111011000010010000101010000011010010110100101001011001000010100100001111110010000110111000110101101011000000010001100101000000110101110110000001100110110011111101000110100101111010001100100110111011111111110101010101110010001110101101101011011011010101101111011101011001010100011111001;
   parameter [539:0] h3sa2 = 540'b011110111000000100011000001011011100011110100100110101100011111100101110001100100001101110110101101010001110000011110110000111011000100011110100000010111100000111001110011011101000110100001101000100101000011111000110010010111110110111100010010011010101011100111101010110000111011010001111101001111101001100101101100011000010110000101110000001010100111110001100101001000101001010100110111000111111010100011100110111000010101001001001100010101000010101000010000010010111000101100111101001100101110011110111010010000000110100100110100000111110;
   parameter [539:0] h3sa3 = 540'b011010100001011010000011011110000110010001111000111101000101011101010100010000110011000011101101001101101110100010111110111111111000110110011010100001010110010001101100010101001000100110101011110010010001010110101101101110100011111111111010100001111111111000111011101010011101010100100100101000111010001011001010111100100100100001000000100111000000001010000011100100110011100110101011110110010100001110111111100111100010111111110101000001111100000011000000000001111111111011110000101110110011011011010111011001101011010001000001110100010111;

   h3hash15 #(.sds(h3sa0)) hashaddr0 (.clk(clk), .resetn, .kin(datain), .inval(dataval), .outval(avalid[0]), .respos(outa[0]));
   h3hash15 #(.sds(h3sa1)) hashaddr1 (.clk(clk), .resetn, .kin(datain), .inval(dataval), .outval(avalid[1]), .respos(outa[1]));
   h3hash15 #(.sds(h3sa2)) hashaddr2 (.clk(clk), .resetn, .kin(datain), .inval(dataval), .outval(avalid[2]), .respos(outa[2]));
   h3hash15 #(.sds(h3sa3)) hashaddr3 (.clk(clk), .resetn, .kin(datain), .inval(dataval), .outval(avalid[3]), .respos(outa[3]));

   memrow20W18 sketchrow0 (.clka(clk), .wea(rowwea[0]), .addra(wrow_addr[0]), .dina(w_data[0]), .clkb(clk), .addrb(rrow_addr[0]), .doutb(r_data[0]));
   memrow20W18 sketchrow1 (.clka(clk), .wea(rowwea[1]), .addra(wrow_addr[1]), .dina(w_data[1]), .clkb(clk), .addrb(rrow_addr[1]), .doutb(r_data[1]));
   memrow20W18 sketchrow2 (.clka(clk), .wea(rowwea[2]), .addra(wrow_addr[2]), .dina(w_data[2]), .clkb(clk), .addrb(rrow_addr[2]), .doutb(r_data[2]));
   memrow20W18 sketchrow3 (.clka(clk), .wea(rowwea[3]), .addra(wrow_addr[3]), .dina(w_data[3]), .clkb(clk), .addrb(rrow_addr[3]), .doutb(r_data[3]));

   minimum estim (.clk, .in0(r_data[0]), .in1(r_data[1]), .in2(r_data[2]), .in3(r_data[3]), .out(est));

   assign hhalert = (est >= 45000)? 1:0;

   genvar  i;

   generate
      for (i = 0; i<4; i++) begin
         assign rrow_addr[i] = outa[i];
         assign rowwea[i] = ((save_data2[i] == est) && save_val3[i]);
         always_ff @(posedge clk) begin
            save_addr[i] <= outa[i];
            save_val[i]  <= avalid[i];

            save_data[i]  <= r_data[i];
            save_addr2[i] <= save_addr[i];
            save_val2[i]  <= save_val[i];

            w_data[i] <= save_data[i]+1;
            wrow_addr[i] <= save_addr2[i];
            save_val3[i] <= save_val2[i];
            save_data2[i] <= save_data[i];
         end // always_ff @ (posedge clk)
      end // for (i = 0; i<4; i++)
   endgenerate

   always_ff @(posedge clk) begin
      kmerSR0 <= datain;
      kmerSR1 <= kmerSR0;
      kmerSR2 <= kmerSR1;
      kmerSR3 <= kmerSR2;
      kmerSR4 <= kmerSR3;
      kmerSR5 <= kmerSR4;
      kmerSR6 <= kmerSR5;
      kmerSR7 <= kmerSR6;

      valSR0 <= dataval;
      valSR1 <= valSR0;
      valSR2 <= valSR1;
      valSR3 <= valSR2;
      valSR4 <= valSR3;
      valSR5 <= valSR4;
      valSR6 <= valSR5;
      valSR7 <= valSR6;

      validHH <= (hhalert && valSR7)? 1'b1 : 1'b0;
      dataout <= {est, kmerSR7};
   end // always_ff @ (posedge clk)
endmodule // sketch
