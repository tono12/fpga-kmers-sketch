module sketch14(
       input  logic        clk,
       input  logic [27:0] datain,
       input  logic        dataval,
       input  logic        resetn,
       output logic        validHH,
       output logic [47:0] dataout
    );


   logic [17:0] outa[0:3];
   logic        avalid[0:3];

   logic [17:0] rrow_addr[0:3];
   logic [17:0] wrow_addr[0:3];
   logic [19:0]  w_data[0:3];
   logic [19:0]  r_data[0:3];
   logic        rowwea[0:3];

   logic [17:0] save_addr[0:3];
   logic [17:0] save_addr2[0:3];
   logic        save_val[0:3];
   logic        save_val2[0:3];
   logic        save_val3[0:3];
   logic [19:0]  save_data[0:3];
   logic [19:0]  save_data2[0:3];

   logic [19:0]  est;
   logic        hhalert;

   logic [27:0] kmerSR0, kmerSR1, kmerSR2, kmerSR3, kmerSR4, kmerSR5, kmerSR6, kmerSR7;
   logic        valSR0, valSR1, valSR2, valSR3, valSR4, valSR5, valSR6, valSR7;

   parameter [503:0] h3sa0 = 504'b111100000001001010001100011000101110011100010100000011101011000111001111010100101100101011011110101101100110011100010001010000110110011110111100011001101110010100101011001111000111010000011011111010100010000111111101001001001110101011111000111001100101101001011000011111010010000010111101011110010010011011110010111010001100000010101101101001101000010100100100110011000101111110101101100001101011100111101110011001001111001010110010001111000111100010110001011011011001111001001010110110110100101001010001;
   parameter [503:0] h3sa1 = 504'b000001010110110000111110011001001000101011010001000011011001101001001000000010000101111010000001111100100111101000110100011110101111101011101110111101111101001110001101001110000000010110011011110000001110100101110101000110111111010001111000111011111011100011010100110000001010011010101000000011100111000100011001011000010111011111110010100000001001100011010100000111101010011001101010111011000110101100000111011000110001001111010100101000000011111001111110010111011000010100010000001101111110110011100000;
   parameter [503:0] h3sa2 = 504'b100011001011010011000111100100000010111101000111101110111011111001000010011111100001010001000111011001101000110011010101001100111001011100010100111100101011110111010100011101000101001111000011110100010101111010110111101010001100010011100101100011011110110011000100100100001110110010100001111001001010111101010111011000101010100110001001010010010100011000110111101101101100111100101101001100110000000111001001010011110011001110010101100011001111100100001001000010000010011111100100011110111100011000110000;
   parameter [503:0] h3sa3 = 504'b111000000110101101100100011110111001011000010011100111111101000011010111001000011101001000001000000100011110100101011010000111001000101111000100010111001110011100110010100010100000001100111101110100001111110110001001010111010010111100010111100101100011111110001000001101101001010010101110011110011010000110011000101110001100111000000100000001011111111111000010110000100011110101000011110010100001101100100101001100100111000001011110001111010001011011111110110110101101000101000011011101001100000101110101;

   h3hash14 #(.sds(h3sa0)) hashaddr0 (.clk(clk), .resetn, .kin(datain), .inval(dataval), .outval(avalid[0]), .respos(outa[0]));
   h3hash14 #(.sds(h3sa1)) hashaddr1 (.clk(clk), .resetn, .kin(datain), .inval(dataval), .outval(avalid[1]), .respos(outa[1]));
   h3hash14 #(.sds(h3sa2)) hashaddr2 (.clk(clk), .resetn, .kin(datain), .inval(dataval), .outval(avalid[2]), .respos(outa[2]));
   h3hash14 #(.sds(h3sa3)) hashaddr3 (.clk(clk), .resetn, .kin(datain), .inval(dataval), .outval(avalid[3]), .respos(outa[3]));

   memrow20W18 sketchrow0 (.clka(clk), .wea(rowwea[0]), .addra(wrow_addr[0]), .dina(w_data[0]), .clkb(clk), .addrb(rrow_addr[0]), .doutb(r_data[0]));
   memrow20W18 sketchrow1 (.clka(clk), .wea(rowwea[1]), .addra(wrow_addr[1]), .dina(w_data[1]), .clkb(clk), .addrb(rrow_addr[1]), .doutb(r_data[1]));
   memrow20W18 sketchrow2 (.clka(clk), .wea(rowwea[2]), .addra(wrow_addr[2]), .dina(w_data[2]), .clkb(clk), .addrb(rrow_addr[2]), .doutb(r_data[2]));
   memrow20W18 sketchrow3 (.clka(clk), .wea(rowwea[3]), .addra(wrow_addr[3]), .dina(w_data[3]), .clkb(clk), .addrb(rrow_addr[3]), .doutb(r_data[3]));

   minimum estim (.clk, .in0(r_data[0]), .in1(r_data[1]), .in2(r_data[2]), .in3(r_data[3]), .out(est));

   assign hhalert = (est >= 45000)? 1:0;

   genvar  i;

   generate
      for (i = 0; i<4; i++) begin
         assign rrow_addr[i] = outa[i];
         assign rowwea[i] = ((save_data2[i] == est) && save_val3[i]);
         always_ff @(posedge clk) begin
            save_addr[i] <= outa[i];
            save_val[i]  <= avalid[i];

            save_data[i]  <= r_data[i];
            save_addr2[i] <= save_addr[i];
            save_val2[i]  <= save_val[i];

            w_data[i] <= save_data[i]+1;
            wrow_addr[i] <= save_addr2[i];
            save_val3[i] <= save_val2[i];
            save_data2[i] <= save_data[i];
         end // always_ff @ (posedge clk)
      end // for (i = 0; i<4; i++)
   endgenerate

   always_ff @(posedge clk) begin
      kmerSR0 <= datain;
      kmerSR1 <= kmerSR0;
      kmerSR2 <= kmerSR1;
      kmerSR3 <= kmerSR2;
      kmerSR4 <= kmerSR3;
      kmerSR5 <= kmerSR4;
      kmerSR6 <= kmerSR5;
      kmerSR7 <= kmerSR6;

      valSR0 <= dataval;
      valSR1 <= valSR0;
      valSR2 <= valSR1;
      valSR3 <= valSR2;
      valSR4 <= valSR3;
      valSR5 <= valSR4;
      valSR6 <= valSR5;
      valSR7 <= valSR6;

      validHH <= (hhalert && valSR7)? 1'b1 : 1'b0;
      dataout <= {est, kmerSR7};
   end // always_ff @ (posedge clk)
endmodule // sketch
