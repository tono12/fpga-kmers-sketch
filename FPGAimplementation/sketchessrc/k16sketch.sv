module sketch16(
       input  logic        clk,
       input  logic [31:0] datain,
       input  logic        dataval,
       input  logic        resetn,
       output logic        validHH,
       output logic [51:0] dataout
    );


   logic [17:0] outa[0:3];
   logic        avalid[0:3];

   logic [17:0] rrow_addr[0:3];
   logic [17:0] wrow_addr[0:3];
   logic [19:0]  w_data[0:3];
   logic [19:0]  r_data[0:3];
   logic        rowwea[0:3];

   logic [17:0] save_addr[0:3];
   logic [17:0] save_addr2[0:3];
   logic        save_val[0:3];
   logic        save_val2[0:3];
   logic        save_val3[0:3];
   logic [19:0]  save_data[0:3];
   logic [19:0]  save_data2[0:3];

   logic [19:0]  est;
   logic        hhalert;

   logic [31:0] kmerSR0, kmerSR1, kmerSR2, kmerSR3, kmerSR4, kmerSR5, kmerSR6, kmerSR7;
   logic        valSR0, valSR1, valSR2, valSR3, valSR4, valSR5, valSR6, valSR7;

   parameter [575:0] h3sa0 = 576'b000001010111110010100110010011001000011110001111101001110101100101111100110010000111001101100001011100101100111101101010110100101010101001110110000111110011111110111110000000100001011101010101010110010110001010100101101101111010001010010101101000000101010100100000001111011101100101010000011000010110100101000111110001101011110000100101110100011101010000101110001100110000001000010000000110011011100001001110101100100101101001110101000100011001001111010011010111000111111101111111001011001010000111111100101110101110000001001100101010101011100010001110111100001110101001010001;
   parameter [575:0] h3sa1 = 576'b010000101011110001000110010100000110001110010010111000101100011001111001100100100011111101101100111100001010100011000110111001010001100010011001000111110000110101010100011111100110110011110000101110000001001000000011000011000101000011111000000101101011111011111010101110101111000110010000110011101101010011011101000001100010000111000110011010101110000011001111111011111101101101101010111000001100100110011110111001110100100000100111010001000100111001100010011100101010110110111110011000001000111000010111001001100111101001011100110001010100101010001101010001011000100111101001;
   parameter [575:0] h3sa2 = 576'b110011110001010100000111000000111101111001001100111100100101100001001101011110000011000000100111011110011000101001011010000101100011010100001110110100010110001000000111100100010110000000001111010101000001000101000010110110101111000101000100001000100110011000101000000111011101001100100000001101011110000001001010001001000100111100110111011010010001001010011100010111010110000110111000100010000000111111001100111000001001100101000111000011111110000100100001001110110011100100110001011101000110011011011110101100111101011100001010010010101001100010101010000011101111000000101101;
   parameter [575:0] h3sa3 = 576'b100111010111000010111011011001000010011101111001100100011110110110111110000111111111101111001001000110100000011101000000100011111000010001001100100111101011100001000110001101001010001110101100110101110111100001011100100001101000101010110100001101001101001110101000011110010001101001101110101000000111110000110101100000010010101100101011111011111110100100111101110000001100111001111110011011101001111110000010010100001001011101111000110010001111101111101011101011000110110000101000101101101001101011010001110101101101010001100111011000000001100100101101101101111100010111010001;

   h3hash16 #(.sds(h3sa0)) hashaddr0 (.clk(clk), .resetn, .kin(datain), .inval(dataval), .outval(avalid[0]), .respos(outa[0]));
   h3hash16 #(.sds(h3sa1)) hashaddr1 (.clk(clk), .resetn, .kin(datain), .inval(dataval), .outval(avalid[1]), .respos(outa[1]));
   h3hash16 #(.sds(h3sa2)) hashaddr2 (.clk(clk), .resetn, .kin(datain), .inval(dataval), .outval(avalid[2]), .respos(outa[2]));
   h3hash16 #(.sds(h3sa3)) hashaddr3 (.clk(clk), .resetn, .kin(datain), .inval(dataval), .outval(avalid[3]), .respos(outa[3]));

   memrow20W18 sketchrow0 (.clka(clk), .wea(rowwea[0]), .addra(wrow_addr[0]), .dina(w_data[0]), .clkb(clk), .addrb(rrow_addr[0]), .doutb(r_data[0]));
   memrow20W18 sketchrow1 (.clka(clk), .wea(rowwea[1]), .addra(wrow_addr[1]), .dina(w_data[1]), .clkb(clk), .addrb(rrow_addr[1]), .doutb(r_data[1]));
   memrow20W18 sketchrow2 (.clka(clk), .wea(rowwea[2]), .addra(wrow_addr[2]), .dina(w_data[2]), .clkb(clk), .addrb(rrow_addr[2]), .doutb(r_data[2]));
   memrow20W18 sketchrow3 (.clka(clk), .wea(rowwea[3]), .addra(wrow_addr[3]), .dina(w_data[3]), .clkb(clk), .addrb(rrow_addr[3]), .doutb(r_data[3]));

   minimum estim (.clk, .in0(r_data[0]), .in1(r_data[1]), .in2(r_data[2]), .in3(r_data[3]), .out(est));

   assign hhalert = (est >= 40000)? 1:0;

   genvar  i;

   generate
      for (i = 0; i<4; i++) begin
         assign rrow_addr[i] = outa[i];
         assign rowwea[i] = ((save_data2[i] == est) && save_val3[i]);
         always_ff @(posedge clk) begin
            save_addr[i] <= outa[i];
            save_val[i]  <= avalid[i];

            save_data[i]  <= r_data[i];
            save_addr2[i] <= save_addr[i];
            save_val2[i]  <= save_val[i];

            w_data[i] <= save_data[i]+1;
            wrow_addr[i] <= save_addr2[i];
            save_val3[i] <= save_val2[i];
            save_data2[i] <= save_data[i];
         end // always_ff @ (posedge clk)
      end // for (i = 0; i<4; i++)
   endgenerate

   always_ff @(posedge clk) begin
      kmerSR0 <= datain;
      kmerSR1 <= kmerSR0;
      kmerSR2 <= kmerSR1;
      kmerSR3 <= kmerSR2;
      kmerSR4 <= kmerSR3;
      kmerSR5 <= kmerSR4;
      kmerSR6 <= kmerSR5;
      kmerSR7 <= kmerSR6;

      valSR0 <= dataval;
      valSR1 <= valSR0;
      valSR2 <= valSR1;
      valSR3 <= valSR2;
      valSR4 <= valSR3;
      valSR5 <= valSR4;
      valSR6 <= valSR5;
      valSR7 <= valSR6;

      validHH <= (hhalert && valSR7)? 1'b1 : 1'b0;
      dataout <= {est, kmerSR7};
   end // always_ff @ (posedge clk)
endmodule // sketch
