module sketch13(
       input  logic        clk,
       input  logic [25:0] datain,
       input  logic        dataval,
       input  logic        resetn,
       output logic        validHH,
       output logic [45:0] dataout
    );


   logic [17:0] outa[0:3];
   logic        avalid[0:3];

   logic [17:0] rrow_addr[0:3];
   logic [17:0] wrow_addr[0:3];
   logic [19:0]  w_data[0:3];
   logic [19:0]  r_data[0:3];
   logic        rowwea[0:3];

   logic [17:0] save_addr[0:3];
   logic [17:0] save_addr2[0:3];
   logic        save_val[0:3];
   logic        save_val2[0:3];
   logic        save_val3[0:3];
   logic [19:0]  save_data[0:3];
   logic [19:0]  save_data2[0:3];

   logic [19:0]  est;
   logic        hhalert;

   logic [25:0] kmerSR0, kmerSR1, kmerSR2, kmerSR3, kmerSR4, kmerSR5, kmerSR6, kmerSR7;
   logic        valSR0, valSR1, valSR2, valSR3, valSR4, valSR5, valSR6, valSR7;

   parameter [467:0] h3sa0 = 468'b101111101101010011000101000100011011101111001100100011101101110101110110001101100110101000101000111011110000001101000100100111111101000000110011010101101110100010011110001011011000111101000010001000101110101111100101101100010100101100101111100101010101110110111110101100101111111111010011000100111100000011010010000000100101101000000101001001000111110011010000000100001001110101011101110001100101101110001001001011111101111001100110101110111001101111111100111011011000;
   parameter [467:0] h3sa1 = 468'b010110001010010000100110100111110010010001110110101110100010000110110101000010111010010000111000011011101101100100110101110001100001011000011101111010001001100111100101111100100101100101000100010000110011100110000100111111101001001001111001001110111001001011011001110111111111111001110011111000001010100011101011101001101011010001011000100100001101110110000001101110010000010110111010011011000110101110010011010100111100001010011111110001000110101001011010000000100110;
   parameter [467:0] h3sa2 = 468'b010110001111010000010010101100111000000100011010100111001011010100111011100111110110111100011011110111000010010010110111000000110011010011001101100000011100000000011001010000010011001110110011100101000001110010010101000100101011110001111000101101111001011011000111010100101000100000000001101000001110101110000011100100100101000000101100111101000010001000000111010000011001110010100110101101010101100111110000011011000001011010011110000101101010000010011011010100010000;
   parameter [467:0] h3sa3 = 468'b101010000111101111101000100010101111001011111101101100110101000011100110011110000001001000000000000011101110001100010001001001011001111011010011111101101000101111110101111101110011001110010100100110110110011010100100010010111010011101001001100010101011010010101011010101101100010100010011110111110101111000000110011000010010100000011000101000110100011101000111011001011111010101000011100011011101100100011100011010001000001110011001111110011111110010101000010101000111;

   h3hash13 #(.sds(h3sa0)) hashaddr0 (.clk(clk), .resetn, .kin(datain), .inval(dataval), .outval(avalid[0]), .respos(outa[0]));
   h3hash13 #(.sds(h3sa1)) hashaddr1 (.clk(clk), .resetn, .kin(datain), .inval(dataval), .outval(avalid[1]), .respos(outa[1]));
   h3hash13 #(.sds(h3sa2)) hashaddr2 (.clk(clk), .resetn, .kin(datain), .inval(dataval), .outval(avalid[2]), .respos(outa[2]));
   h3hash13 #(.sds(h3sa3)) hashaddr3 (.clk(clk), .resetn, .kin(datain), .inval(dataval), .outval(avalid[3]), .respos(outa[3]));

   memrow20W18 sketchrow0 (.clka(clk), .wea(rowwea[0]), .addra(wrow_addr[0]), .dina(w_data[0]), .clkb(clk), .addrb(rrow_addr[0]), .doutb(r_data[0]));
   memrow20W18 sketchrow1 (.clka(clk), .wea(rowwea[1]), .addra(wrow_addr[1]), .dina(w_data[1]), .clkb(clk), .addrb(rrow_addr[1]), .doutb(r_data[1]));
   memrow20W18 sketchrow2 (.clka(clk), .wea(rowwea[2]), .addra(wrow_addr[2]), .dina(w_data[2]), .clkb(clk), .addrb(rrow_addr[2]), .doutb(r_data[2]));
   memrow20W18 sketchrow3 (.clka(clk), .wea(rowwea[3]), .addra(wrow_addr[3]), .dina(w_data[3]), .clkb(clk), .addrb(rrow_addr[3]), .doutb(r_data[3]));

   minimum estim (.clk, .in0(r_data[0]), .in1(r_data[1]), .in2(r_data[2]), .in3(r_data[3]), .out(est));

   assign hhalert = (est >= 45000)? 1:0;

   genvar  i;

   generate
      for (i = 0; i<4; i++) begin
         assign rrow_addr[i] = outa[i];
         assign rowwea[i] = ((save_data2[i] == est) && save_val3[i]);
         always_ff @(posedge clk) begin
            save_addr[i] <= outa[i];
            save_val[i]  <= avalid[i];

            save_data[i]  <= r_data[i];
            save_addr2[i] <= save_addr[i];
            save_val2[i]  <= save_val[i];

            w_data[i] <= save_data[i]+1;
            wrow_addr[i] <= save_addr2[i];
            save_val3[i] <= save_val2[i];
            save_data2[i] <= save_data[i];
         end // always_ff @ (posedge clk)
      end // for (i = 0; i<4; i++)
   endgenerate

   always_ff @(posedge clk) begin
      kmerSR0 <= datain;
      kmerSR1 <= kmerSR0;
      kmerSR2 <= kmerSR1;
      kmerSR3 <= kmerSR2;
      kmerSR4 <= kmerSR3;
      kmerSR5 <= kmerSR4;
      kmerSR6 <= kmerSR5;
      kmerSR7 <= kmerSR6;

      valSR0 <= dataval;
      valSR1 <= valSR0;
      valSR2 <= valSR1;
      valSR3 <= valSR2;
      valSR4 <= valSR3;
      valSR5 <= valSR4;
      valSR6 <= valSR5;
      valSR7 <= valSR6;

      validHH <= (hhalert && valSR7)? 1'b1 : 1'b0;
      dataout <= {est, kmerSR7};
   end // always_ff @ (posedge clk)
endmodule // sketch
