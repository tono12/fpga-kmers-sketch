module sketch17(
       input  logic        clk,
       input  logic [33:0] datain,
       input  logic        dataval,
       input  logic        resetn,
       output logic        validHH,
       output logic [53:0] dataout
    );


   logic [17:0] outa[0:3];
   logic        avalid[0:3];

   logic [17:0] rrow_addr[0:3];
   logic [17:0] wrow_addr[0:3];
   logic [19:0]  w_data[0:3];
   logic [19:0]  r_data[0:3];
   logic        rowwea[0:3];

   logic [17:0] save_addr[0:3];
   logic [17:0] save_addr2[0:3];
   logic        save_val[0:3];
   logic        save_val2[0:3];
   logic        save_val3[0:3];
   logic [19:0]  save_data[0:3];
   logic [19:0]  save_data2[0:3];

   logic [19:0]  est;
   logic        hhalert;

   logic [33:0] kmerSR0, kmerSR1, kmerSR2, kmerSR3, kmerSR4, kmerSR5, kmerSR6, kmerSR7, kmerSR8;
   logic        valSR0, valSR1, valSR2, valSR3, valSR4, valSR5, valSR6, valSR7, valSR8;

   parameter [611:0] h3sa0 = 612'b011100000110100011110100010000010101011011101000110110010010100000101111010001110010010101001100101100110011010110100101000110100001000100100110111010111001011000111001011010010110010110111100010011011111101011001110110000110000010001100111001101011100110100010100111100110101010001101101000011101110001100010100001011001000000000001111111001111010001101011110001101110110010011010001011101010111101101011111010011111001110011011101001111011010100011110111010101011110100011010010110010001100000101001110000100101100111011001001001001011001100100011100010110010111100000000000110100111000001111101001101111101111;
   parameter [611:0] h3sa1 = 612'b000111111111110101110101100000000101101010111001011100000101010111111011000000111011101111010001110001100010111100101101011000010000100111001011111001101000011111010000101000010111100001100010111000111100001001110001110001100101011100100010011101001000101000101100101000101000111111111011100101001101110000101011111110000010000010000000101100011011001001100110010100111100110111110010001001011000100110100011000000010111111100101101001011100001010110100111110011100100100110001010010100000101010000110010010101000011100011111100001011110000100001011110100100010001011010011101111000111011010010110110111001100000;
   parameter [611:0] h3sa2 = 612'b010011001111100001011001001000101111110100110001110100101000010001110100100110110011010111000101010101111000100011110010101111011001101001010000110101111011101000010100101101001001111011100001010101001010001001101101011010100111100110001011011000100110000001101100000011001110110000011111010000001100000001100010001111001111011010110101100101000100011110110111110101101010110001010011110001001101111111111110100100101100001010010101100110101101110001110100000101100011011010010110110011000011011001111100110011000010001110110111101011101111110101100110001111100110011101110110000010100101111010110100001110111010;
   parameter [611:0] h3sa3 = 612'b011101011001010000101111101011010000111010101000111101100001100101011110001010000101101110111000011110001110001111101100101010000101010111000100111011101110100000010111110100011000101000101010100011011011111110111111101010100010110110010000001111001000010000111000110111101110111000010101100000011110100011010100101101001010001010100111000000000010010001000010101001000010100111111011110100001110001001100111100011000100011111101101101100110110011011000101101000001001010011011011001010111111000010001101010111110011110111010000001010010011110000110001000001010100100110110101101101001000000001010000000110111101;

   h3hash17 #(.sds(h3sa0)) hashaddr0 (.clk(clk), .resetn, .kin(datain), .inval(dataval), .outval(avalid[0]), .respos(outa[0]));
   h3hash17 #(.sds(h3sa1)) hashaddr1 (.clk(clk), .resetn, .kin(datain), .inval(dataval), .outval(avalid[1]), .respos(outa[1]));
   h3hash17 #(.sds(h3sa2)) hashaddr2 (.clk(clk), .resetn, .kin(datain), .inval(dataval), .outval(avalid[2]), .respos(outa[2]));
   h3hash17 #(.sds(h3sa3)) hashaddr3 (.clk(clk), .resetn, .kin(datain), .inval(dataval), .outval(avalid[3]), .respos(outa[3]));

   memrow20W18 sketchrow0 (.clka(clk), .wea(rowwea[0]), .addra(wrow_addr[0]), .dina(w_data[0]), .clkb(clk), .addrb(rrow_addr[0]), .doutb(r_data[0]));
   memrow20W18 sketchrow1 (.clka(clk), .wea(rowwea[1]), .addra(wrow_addr[1]), .dina(w_data[1]), .clkb(clk), .addrb(rrow_addr[1]), .doutb(r_data[1]));
   memrow20W18 sketchrow2 (.clka(clk), .wea(rowwea[2]), .addra(wrow_addr[2]), .dina(w_data[2]), .clkb(clk), .addrb(rrow_addr[2]), .doutb(r_data[2]));
   memrow20W18 sketchrow3 (.clka(clk), .wea(rowwea[3]), .addra(wrow_addr[3]), .dina(w_data[3]), .clkb(clk), .addrb(rrow_addr[3]), .doutb(r_data[3]));

   minimum estim (.clk, .in0(r_data[0]), .in1(r_data[1]), .in2(r_data[2]), .in3(r_data[3]), .out(est));

   assign hhalert = (est >= 40000)? 1:0;

   genvar  i;

   generate
      for (i = 0; i<4; i++) begin
         assign rrow_addr[i] = outa[i];
         assign rowwea[i] = ((save_data2[i] == est) && save_val3[i]);
         always_ff @(posedge clk) begin
            save_addr[i] <= outa[i];
            save_val[i]  <= avalid[i];

            save_data[i]  <= r_data[i];
            save_addr2[i] <= save_addr[i];
            save_val2[i]  <= save_val[i];

            w_data[i] <= save_data[i]+1;
            wrow_addr[i] <= save_addr2[i];
            save_val3[i] <= save_val2[i];
            save_data2[i] <= save_data[i];
         end // always_ff @ (posedge clk)
      end // for (i = 0; i<4; i++)
   endgenerate

   always_ff @(posedge clk) begin
      kmerSR0 <= datain;
      kmerSR1 <= kmerSR0;
      kmerSR2 <= kmerSR1;
      kmerSR3 <= kmerSR2;
      kmerSR4 <= kmerSR3;
      kmerSR5 <= kmerSR4;
      kmerSR6 <= kmerSR5;
      kmerSR7 <= kmerSR6;
      kmerSR8 <= kmerSR7;

      valSR0 <= dataval;
      valSR1 <= valSR0;
      valSR2 <= valSR1;
      valSR3 <= valSR2;
      valSR4 <= valSR3;
      valSR5 <= valSR4;
      valSR6 <= valSR5;
      valSR7 <= valSR6;
      valSR8 <= valSR7;

      validHH <= (hhalert && valSR8)? 1'b1 : 1'b0;
      dataout <= {est, kmerSR8};
   end // always_ff @ (posedge clk)
endmodule // sketch
