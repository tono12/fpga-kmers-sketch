module cachecontroller11(
       input  logic         clk,
       input  logic         datavalid,
       input  logic         state,
       input  logic [7:0]   queryin,
       input  logic [7:0]   resetn,
       input  logic [41:0]  datain,
       output logic [255:0] outres
    );


   logic [7:0]  setaddr, readaddr, addrb;
   logic        inval;
   logic        outval;
   logic [3:0]  wen, wea;
   logic [7:0]  w_addr;
   logic [41:0] w_data;
   logic [41:0] r_data[0:3];
   logic [3:0]  hit;
   logic [1:0]  ccounter[0:255];
   logic [41:0] cacheSR0, cacheSR1, cacheSR2, cacheSR3, cacheSR4, cacheSR5;
   logic [7:0]  addressSR0;
   logic        validSR0;

   parameter [175:0] h3scache = 176'b11001011001100010101101110111001011011010001100101111101001010011000100101010011001000111111111101000101011010110011101100100011100101010010001110001001100011010110100101111101;

   h3cache11 #(.sds(h3scache)) h3cache (.clk(clk), .kin(datain[21:0]), .resetn, .inval(datavalid), .outval(outval), .respos(setaddr));

   cachek11 cacheset0 (.clka(clk), .wea(wea[0]), .addra(w_addr), .dina(w_data), .clkb(clk), .addrb, .doutb(r_data[0]));
   cachek11 cacheset1 (.clka(clk), .wea(wea[1]), .addra(w_addr), .dina(w_data), .clkb(clk), .addrb, .doutb(r_data[1]));
   cachek11 cacheset2 (.clka(clk), .wea(wea[2]), .addra(w_addr), .dina(w_data), .clkb(clk), .addrb, .doutb(r_data[2]));
   cachek11 cacheset3 (.clka(clk), .wea(wea[3]), .addra(w_addr), .dina(w_data), .clkb(clk), .addrb, .doutb(r_data[3]));

   always_ff @(posedge clk) begin
      cacheSR0 <= datain;
      cacheSR1 <= cacheSR0;
      cacheSR2 <= cacheSR1;
      cacheSR3 <= cacheSR2;
      cacheSR4 <= cacheSR3;
      cacheSR5 <= cacheSR4;

      addressSR0 <= setaddr;
      validSR0 <= outval;

      if (validSR0) begin
         if (hit == 4'b0000) begin
            unique casez (ccounter[addressSR0]) 
               2'b00 : wen <= 4'b1000;
               2'b01 : wen <= 4'b0100;
               2'b10 : wen <= 4'b0010;
               2'b11 : wen <= 4'b0001;
            endcase
            ccounter[addressSR0] <= ccounter[addressSR0] + 1;
         end // if (hit == 3'b000)
         else if (hit == 4'b1111)
            wen <= 4'b0000;
         else
            wen <= hit;
         w_data <= cacheSR5;
         w_addr <= addressSR0;
      end // if (validSR0)
      else begin
         wen <= 4'b0000;
      end // else: !if(validSR0)
   end // always_ff @ (posedge clk)

   assign addrb = (state)? readaddr:setaddr;
   assign wea = (state)? 4'b0000:wen;

   always_comb begin
      readaddr =  queryin;
      outres = {22'd0, r_data[0], 22'd0, r_data[1], 22'd0, r_data[2], 22'd0, r_data[3]};
   end
   genvar i, k;
   generate
      for (i = 0; i<4; i++) begin
         assign hit[i] = (r_data[i][21:0] == cacheSR5[21:0]);
      end // for (i = 0; i<2; i++)
   endgenerate

   generate
      for (k = 0; k<256; k++) begin
         initial ccounter[k] = 0;
      end // for (k++)
   endgenerate
endmodule // sketch
