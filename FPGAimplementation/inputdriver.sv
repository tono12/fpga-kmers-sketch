////////////////////////////////////////////////////////////////////////////////

// default_nettype of none prevents implicit wire declaration.
`default_nettype none
`timescale 1ps / 1ps

module inputdriver #(
  parameter integer C_AXIS_TDATA_WIDTH = 512, // Data width of input
  parameter integer C_ADDER_BIT_WIDTH  = 32
)
(
  input wire 				aclk,
  input wire 				aresetn,

  input wire 				s_axis_tvalid,
  output reg 				s_axis_tready,
  input wire [C_AXIS_TDATA_WIDTH-1:0] 	s_axis_tdata,
  input wire [C_AXIS_TDATA_WIDTH/8-1:0] s_axis_tkeep,
  input wire 				s_axis_tlast,

  output logic 				valid,
  output logic [399:0] 			outdata
 
);

   
/////////////////////////////////////////////////////////////////////////////
// Variables
/////////////////////////////////////////////////////////////////////////////

reg                            areset = 1'b0;
reg   [1:0]                    fsm_state;

logic                          wea_outbuff;
logic [1:0]                    addr_woutbuff, addr_routbuff; // 4:0
logic [399:0]                  dina_outbuff, doutb;

logic 			       outputvalid, lasttransf, dataval, endlast;
logic [7:0]                    counter;
      
/////////////////////////////////////////////////////////////////////////////
// Output buffer
/////////////////////////////////////////////////////////////////////////////

readbuff output_buffer (
  .clka(aclk),            // input wire clka
  .wea(wea_outbuff),      // input wire [0 : 0] wea
  .addra(addr_woutbuff),  // input wire [1 : 0] addra
  .dina(dina_outbuff),    // input wire [399 : 0] dina
                                  
  .clkb(aclk),            // input wire clkb
  .addrb(addr_routbuff),  // input wire [1 : 0] addrb
  .doutb(doutb)           // output wire [399 : 0] doutb
);

/////////////////////////////////////////////////////////////////////////////
// Reset Logic
/////////////////////////////////////////////////////////////////////////////

always @(posedge aclk) begin
  areset <= ~aresetn;
end

always @(posedge aclk) begin
   if (areset) begin
      fsm_state <= 2'b00;
      s_axis_tready <= 1'b0;
      wea_outbuff <= 1'b0;
      outputvalid <= 1'b0;
      lasttransf <= 1'b0;
      dataval <= 1'b0;
      endlast <= 1'b0;
      counter <= 8'd0;
      addr_woutbuff <= 2'd0; // 5
      addr_routbuff <= 2'd0;
   end
end

/////////////////////////////////////////////////////////////////////////////
// FSM axi-read buffer write logic
/////////////////////////////////////////////////////////////////////////////
   
always_ff @(posedge aclk) begin
   if (s_axis_tlast) begin
      lasttransf <= 1'b1;
   end
   case (fsm_state)
     2'b00: begin
	s_axis_tready <= 1'b1;
	fsm_state <= 2'b01;
     end
     2'b01: begin // lectura dato valido
	if (s_axis_tvalid) begin
	   dina_outbuff <= s_axis_tdata[399:0];
	   fsm_state <= 2'b10;
	   s_axis_tready <= 1'b0;
	   wea_outbuff <= 1'b1;
	end
     end
     2'b10: begin
	addr_woutbuff <= addr_woutbuff + 1'b1;
	wea_outbuff <= 1'b0;
	fsm_state <= 2'b11;
	outputvalid <= 1'b1;
     end
     2'b11: begin
	if (addr_woutbuff != addr_routbuff) begin
	   fsm_state <= 2'b01;
	   s_axis_tready <= 1'b1;
	end
     end
   endcase // case (fsm_state)
end // always @ (posedge aclk)

/////////////////////////////////////////////////////////////////////////////
// buffer read 2 output logic
/////////////////////////////////////////////////////////////////////////////

always_ff @(posedge aclk) begin
   if (outputvalid^endlast) begin
      outdata <= doutb;
      counter <= counter + 1'b1;
      dataval <= 1'b1;
      if (counter == 8'd12) begin // 188
	 counter <= 8'd0;
	 addr_routbuff <= addr_routbuff + 1'b1;
	 if (lasttransf) begin
	    endlast <= 1'b1;
	 end
      end
   end
end // always_ff @ (posedge aclk)

assign valid = dataval^endlast;
   
endmodule
