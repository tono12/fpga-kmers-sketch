module minimum(
       input  logic       clk,
       input  logic [19:0] in0, in1, in2, in3,
       output logic [19:0] out
    );

   logic [19:0] s1_0, s1_1;

   always_ff @(posedge clk) begin
      if (in1 > in0) begin
         s1_0 <= in0;
      end
      else begin
         s1_0 <= in1;
      end
      if (in3 > in2) begin
         s1_1 <= in2;
      end
      else begin
         s1_1 <= in3;
      end
      out <= (s1_0 > s1_1)? s1_1 : s1_0;
   end // always_ff @ (posedge clk)
endmodule
