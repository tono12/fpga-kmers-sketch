<h1 align="center">Parallel Software Implementation </h1>
<p>
  <a href="#" target="_blank">
    <img alt="License: GPLv3.0" src="https://img.shields.io/badge/License-GPLv3.0-yellow.svg" />
  </a>
</p>

Sofware sketch implementations for "Mining discriminative k-mers" using countmin-cu sketch sequentially and with AVX support

## Compile

To compile execute `make`

## Script to run experiments 

The script `run-eval.py` executes automatically experiments defined in 
datasets.json file. The structure of the datasets.json is as follows:

```json
{
    "<datasetname>": {
        "tags": ["default"],  // labels to group datasets 
        "test_file": "path/test-dataset",
        "control_file": "path/control-dataset",
        "first_length": min-k,  // i.e. 10
        "thresholds": [<values>, <of>, <thresholds>]
    },
    // ...
}
```

The scripts runs with python3

```
python3 run_eval.py runs [--program-type type1 [...]] \
[--data-tags tag1 [...]]
```

The parameters are:

* `runs`: number of executions for each case 
* `--program-type`: selects the name of the execution type to run, which can be default, which is the sequential implementation, and avx which is the avx implementation 
* `--data-tags`: selects the datasets to run, based on labels in datasets.json specified in the "tags" given for a dataset. If not given, it will execute all datasets defined in datasets.json

An example of execution is given below. This will execute three times all datasets defined in datasets.json with sequential implementation. The results will be stored in a file in the out directory. If the out directory does not exist, the script will create it.

```
python3 run_eval.py 3 --program-type default
```

The next example run the datasets in datasets.json three times using the avx implementation 

```
python3 run_eval.py 3 --program-type avx 
```

The next example shows how to run the script to execute the avx implementation uusing the dataset ctcf

```
python3 run_eval.py 3 --program-type avx --data-tags ctcf
```
